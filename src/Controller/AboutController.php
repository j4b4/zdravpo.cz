<?php

namespace App\Controller;

use App\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends BasicController
{
    /**
     * @Route("/o-firme", name="about")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Company::class);

        $companies = $repo->findAll();

        return $this->render('about/index.html.twig', [
            'controller_name' => 'AboutController',
            'companies' => $companies,
            'openHours' => $this->getOpenHours()
        ]);
    }
}
