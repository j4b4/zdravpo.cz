<?php

namespace App\Controller;

use App\Entity\ProductCategory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CatalogController extends BasicController
{
    /**
     * @Route("/produkty", name="catalog")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(ProductCategory::class);

        $categories = $repo->findBy(['parent' => null], ['order' => 'ASC']);

        return $this->render('catalog/index.html.twig', [
            'categories' => $categories,
            'controller_name' => 'CatalogController',
            'openHours' => $this->getOpenHours()
        ]);
    }

    /**
     * @Route("/produkty/{categoryUrl}", name="category")
     */
    public function category($categoryUrl)
    {
        $repo = $this->getDoctrine()->getRepository(ProductCategory::class);
        $category = $repo->findOneBy(['url' => $categoryUrl]);

        $subCategories = $repo->findBy(['parent' => $category], ['order' => 'ASC']);

        return $this->render('catalog/category.html.twig', [
            'subCategories' => $subCategories,
            'category' => $category,
            'controller_name' => $category->getName(),
            'openHours' => $this->getOpenHours()
        ]);
    }
    /**
     * @Route("/produkty/{categoryUrl}/{subCategoryUrl}", name="subcategory")
     */
    public function subcategory($categoryUrl, $subCategoryUrl)
    {
        $repo = $this->getDoctrine()->getRepository(ProductCategory::class);
        $subCategory = $repo->findOneBy(['url' => $subCategoryUrl]);
        $category = $repo->findOneBy(['url' => $categoryUrl]);
        $subCategories = $repo->findBy(['parent' => $category], ['order' => 'ASC']);

        return $this->render('catalog/subcategory.html.twig', [
            'category' => $category,
            'subCategory' => $subCategory,
            'subCategories' => $subCategories,
            'controller_name' => $subCategory->getName(),
            'openHours' => $this->getOpenHours()
        ]);
    }
}
