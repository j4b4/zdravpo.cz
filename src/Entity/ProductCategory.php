<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use EasySlugger\SeoSlugger;

/**
 * Class ProductCategory
 * @package App\Entity
 * @ORM\Entity()
 */
class ProductCategory {

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=500, nullable=true)
     * @var string|null
     */
    protected $name;

    /**
     * @ORM\Column(name="url", type="string", length=500, nullable=true)
     * @var string|null
     */
    protected $url;

    /**
     * @var ProductCategory|null
     * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="subCategories")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    protected $parent;

    /**
     * @var ProductCategory[]|Collection
     * @OneToMany(targetEntity="ProductCategory", mappedBy="parent")
     *
     */
    protected $subCategories;

    /**
     * @var ArrayCollection|CompanyProduct[]
     * @OneToMany(targetEntity="CompanyProduct", mappedBy="category", cascade={"persist", "remove"})
     */
    protected $companyProducts;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, name="order_number")
     */
    protected $order = 0;

    public function __construct() {
        $this->companyProducts = new ArrayCollection();
        $this->subCategories = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void {
        $this->name = $name;
        $this->url = SeoSlugger::slugify($name);
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string {
        return $this->url;
    }

    /**
     * @param string|null $url
     */
    public function setUrl(?string $url): void {
        $this->url = $url;
    }

    /**
     * @return ProductCategory|null
     */
    public function getParent(): ?ProductCategory {
        return $this->parent;
    }

    /**
     * @param ProductCategory|null $parent
     */
    public function setParent(?ProductCategory $parent): void {
        $this->parent = $parent;
    }

    /**
     * @return CompanyProduct[]|ArrayCollection
     */
    public function getCompanyProducts() {
        return $this->companyProducts;
    }

    /**
     * @param CompanyProduct[]|ArrayCollection $companyProducts
     */
    public function setCompanyProducts(array $companyProducts): void {
        $this->companyProducts = $companyProducts;
    }

    /**
     * @param CompanyProduct $product
     */
    public function addCompanyProduct(CompanyProduct $product): void {
        if (!$this->companyProducts->contains($product)) {
            $this->companyProducts->add($product);
            $product->setCategory($this);
        }
    }

    /**
     * @param CompanyProduct $product
     */
    public function removeCompanyProduct(CompanyProduct $product): void {
        if ($this->companyProducts->contains($product)) {
            $this->companyProducts->removeElement($product);
        }
    }

    /**
     * @return ProductCategory[]|Collection
     */
    public function getSubCategories() {
        return $this->subCategories;
    }

    /**
     * @param ProductCategory[]|Collection $subCategories
     */
    public function setSubCategories($subCategories): void {
        $this->subCategories = $subCategories;
    }



    /**
     * @return int
     */
    public function getOrder(): int {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void {
        $this->order = $order;
    }

    public function __toString() {
        return $this->name;
    }

}
