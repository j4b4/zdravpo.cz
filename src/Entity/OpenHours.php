<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class OpenHours
 * @package App\Entity
 * @ORM\Entity()
 */
class OpenHours {

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $po;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $ut;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $st;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $ct;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $pa;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $so;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $ne;

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getPo(): ?string {
        return $this->po;
    }

    /**
     * @param string|null $po
     */
    public function setPo(?string $po): void {
        $this->po = $po;
    }

    /**
     * @return string|null
     */
    public function getUt(): ?string {
        return $this->ut;
    }

    /**
     * @param string|null $ut
     */
    public function setUt(?string $ut): void {
        $this->ut = $ut;
    }

    /**
     * @return string|null
     */
    public function getSt(): ?string {
        return $this->st;
    }

    /**
     * @param string|null $st
     */
    public function setSt(?string $st): void {
        $this->st = $st;
    }

    /**
     * @return string|null
     */
    public function getCt(): ?string {
        return $this->ct;
    }

    /**
     * @param string|null $ct
     */
    public function setCt(?string $ct): void {
        $this->ct = $ct;
    }

    /**
     * @return string|null
     */
    public function getPa(): ?string {
        return $this->pa;
    }

    /**
     * @param string|null $pa
     */
    public function setPa(?string $pa): void {
        $this->pa = $pa;
    }

    /**
     * @return string|null
     */
    public function getSo(): ?string {
        return $this->so;
    }

    /**
     * @param string|null $so
     */
    public function setSo(?string $so): void {
        $this->so = $so;
    }

    /**
     * @return string|null
     */
    public function getNe(): ?string {
        return $this->ne;
    }

    /**
     * @param string|null $ne
     */
    public function setNe(?string $ne): void {
        $this->ne = $ne;
    }
}
